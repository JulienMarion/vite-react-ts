export const BASE_URL: string = "http://localhost:5000";

export const ENTITIES = {
    ARTICLES: 'items'
};

export const ARTICLES_CONFIG = {
    DESCRIPTION_LENGTH: 30
};