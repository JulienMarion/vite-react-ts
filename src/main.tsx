import React from 'react';
import ReactDOM from 'react-dom';
import App from './shared/App';
import './config/defaultStyles.css';


ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById( 'root' )
);
