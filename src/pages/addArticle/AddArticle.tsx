import { FormContextProvider } from "./context";
import { Form } from "./components/";

const AddArticle = () => (
    <FormContextProvider>
        <Form />
    </FormContextProvider>
);

export default AddArticle;
