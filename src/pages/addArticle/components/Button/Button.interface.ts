import { MouseEventHandler } from "react";

export interface ButtonProps {
    title: string,
    func?: MouseEventHandler<HTMLButtonElement>;
}