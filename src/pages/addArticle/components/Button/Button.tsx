import { ButtonProps } from "./Button.interface";
import styles from './Button.module.css';

const Button = ( { title, func = () => { } }: ButtonProps ) => (
    <div className={ styles.wrapper }>
        <button
            className={ styles.button }
            onClick={ func }
        >
            { title }
        </button>

    </div>
);

export default Button;