import { LabbeledField } from '..';
import { Article } from "../../../../shared/interfaces";

const FieldList = ( { article }: { article: Article; } ) => (
    <>
        { Object.entries( article ).map(
            ( field, i ) => (
                <LabbeledField
                    field={ field }
                    key={ `${ field[ 0 ] }-${ i }` }
                />
            )
        ) }
    </>
);

export default FieldList;