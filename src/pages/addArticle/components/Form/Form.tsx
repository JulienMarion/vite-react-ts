import { useFormContext } from "../../hooks/useFormContext";
import { Button, FieldList } from '../';
import Snackbar from "../../../../shared/components/Snackbar/Snackbar";

import styles from './Form.module.css';

const Form = () => {

    const { state, snackbarRef, handleReset, handleSubmit } = useFormContext();

    return (
        <div className={ styles.container }>
            <form
                onSubmit={ handleSubmit }
                className={ styles.form }
            >
                <FieldList article={ state } />
                <Button title="Add" />
            </form>
            <Button title="Reset" func={ handleReset } />
            <Snackbar message="New article added" ref={ snackbarRef } />
        </div>
    );
};

export default Form;