export interface LabelledFieldProps {
    field: [
        property: string,
        value: string | boolean | undefined
    ];
}

export interface Field {
    state: any;
    handleChange: Function;
    property: string;
}