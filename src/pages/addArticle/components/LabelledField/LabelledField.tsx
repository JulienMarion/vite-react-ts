import { useFormContext } from "../../hooks/useFormContext";
import { Field, LabelledFieldProps } from "./LabelledField.interface";
import styles from './LabelledField.module.css';

const TextField = ( { state, handleChange, property }: Field ) => (
    <div className={ styles.wrapper }>
        <label
            htmlFor={ property }
            className={ styles.label }
        >
            { property }
        </label>
        <input
            type='text'
            name={ property }
            id={ property }
            className={ styles.input }
            value={ state[ property ] }
            onChange={ e => handleChange( property, e.currentTarget.value ) }
        />
    </div>
);

const CheckboxField = ( { state, handleChange, property }: Field ) => (
    <div className={ styles.wrapper_box }>
        <input
            type='checkbox'
            name={ property }
            id={ property }
            className={ styles.checkbox }
            checked={ state[ property ] }
            onChange={ e => handleChange( property, e.currentTarget.checked ) }
        />
        <label
            htmlFor={ property }
            className={ styles.label }
        >
            { property }
        </label>
    </div>
);

const LabbeledField = ( { field: [ property, value ] }: LabelledFieldProps ) => {

    const { state, handleChange } = useFormContext();

    const fieldprops = { state, handleChange, property };

    return typeof value === 'boolean'
        ? ( <CheckboxField { ...fieldprops } /> )
        : ( <TextField  { ...fieldprops } /> );
};

export default LabbeledField;
