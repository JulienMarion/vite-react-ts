import { createContext, FormEvent, ReactNode } from "react";
import useFormAction from "../hooks/useFormAction";

const FormContext = createContext( {

    state: {} as any,
    snackbarRef: {} as React.MutableRefObject<null>,
    handleChange: ( key: string, value: string | boolean ) => { },
    handleReset: () => { },
    handleSubmit: ( e: FormEvent ) => { }

} );

const FormContextProvider = ( { children }: { children: ReactNode; } ) => {

    const { providerProps } = useFormAction();

    return (
        <FormContext.Provider value={ providerProps }>
            { children }
        </FormContext.Provider>
    );
};

export { FormContext, FormContextProvider };