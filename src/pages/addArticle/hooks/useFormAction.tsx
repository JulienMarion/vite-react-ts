import { FormEvent, useReducer, useRef } from "react";
import { createArticle } from "../../../scripts/queries";
import { article } from "../../../shared/fixtures";
import { Article } from "../../../shared/interfaces";

/* ================================== */
/*             INTERFACES             */
/* ================================== */

type FormAction = {
    key: string,
    type: string,
    value?: string | boolean;
};

const useFormAction = () => {

    const { name, description, createdBy, isActive } = article;

    const initialFormState: Partial<Article> = { name, description, createdBy, isActive };

    const formReducer = ( state: any, { key, type, value }: FormAction ) => ( {
        initial: initialFormState,
        update: { ...state, [ key ]: value },
    } )[ type ];

    const [ state, formDispatch ] = useReducer( formReducer, initialFormState );

    const snackbarRef = useRef( null );

    /* ================================== */
    /*               METHODS              */
    /* ================================== */

    const handleChange = ( key: string, value: string | boolean ) => {
        formDispatch( { type: 'update', key, value } );
    };

    const handleReset = () => { formDispatch( { type: 'initial', key: 'null' } ); };

    const handleSubmit = ( e: FormEvent ) => {

        e.preventDefault();

        const form = e.target as HTMLFormElement;
        const formData = new FormData( form );
        const data = Object.fromEntries( formData.entries() );

        // createArticle( ( data as unknown ) as Article );
        snackbarRef.current.show();

    };

    const providerProps = { state, snackbarRef, handleChange, handleSubmit, handleReset };

    return { providerProps };
};

export default useFormAction;
