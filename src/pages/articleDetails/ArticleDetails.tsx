import { useLocation } from "react-router-dom";
import { Item } from "../../shared/interfaces";

const ArticleDetails = () => {

    const { state } = useLocation();
    const { id, name, description, createdAt, createdBy, isActive } = state as Item;

    return (
        <>
            <div>{ id }</div>
            <div>{ name }</div>
            <div>{ description }</div>
            <div>{ createdAt }</div>
            <div>{ createdBy }</div>
            <div>{ isActive.toString() }</div>
        </>
    );
};

export default ArticleDetails;