import { Item } from "../../shared/interfaces";
import { useQuery } from "react-query";
import { useSearch } from "./hooks";
import { getArticles } from '../../scripts/queries';
import { SearchBarProps } from "./components/SearchBar/SearchBar.interface";
import List from "../../shared/components/List/List";
import Article from "./components/Article/Article";
import SearchBar from "./components/SearchBar/SearchBar";

type RQ = { isLoading: boolean, isError: boolean, error: string | null, data: any; };

const Articles = () => {

    const { isLoading, isError, error, data }: RQ = useQuery( 'repoData', () => getArticles() );

    const { search, setSearch, isActiveOnly, setIsActiveOnly, filteredData } = useSearch<Item>( data );

    const searchBarProps: SearchBarProps<Item> = {
        search,
        filteredData,
        isActiveOnly,
        setSearch,
        setIsActiveOnly
    };

    if ( isError ) { return ( <p>{ error }</p> ); }

    if ( isLoading ) { return ( <p>Loading...</p> ); }

    return (
        <>
            <SearchBar { ...searchBarProps } />
            <List data={ filteredData }>
                { Article }
            </List>
        </>
    );

};

export default Articles;