import { Link } from "react-router-dom";
import { Item } from "../../../../shared/interfaces";
import { ARTICLES_CONFIG } from '../../../../config/constants';
import { useTruncate } from "../../../../shared/hooks";
import Card from "../../../../shared/components/Card/Card";
import styles from './article.module.css';

const Article = ( item: Item ) => {

    const { hasToBeTruncate } = useTruncate();

    const { id, name, description, createdBy, createdAt, isActive } = item;

    return (

        <Link to={ `/articles/${ id }` } state={ { ...item } }>
            <Card >
                <div className={ styles.container }>
                    <h2 className={ styles.h2 }>{ name }</h2>
                    <div className={ styles.container }>
                        <label htmlFor="isActive">active:</label>
                        <input type="checkbox" name="isActive" readOnly checked={ isActive } />
                    </div>
                </div>
                <div className={ styles.hr } />
                <p className={ styles.p }>
                    { hasToBeTruncate( description, ARTICLES_CONFIG.DESCRIPTION_LENGTH ) }
                </p>
                <div className={ styles.hr } />
                <div className={ styles.container }>
                    <q >{ createdBy }</q>
                    <p >{ createdAt }</p>
                </div>
            </Card>
        </Link>
    );
};

export default Article;