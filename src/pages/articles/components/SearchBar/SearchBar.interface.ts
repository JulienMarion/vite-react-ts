
export interface SearchBarProps<T> {
    search: string,
    filteredData: T[],
    isActiveOnly: boolean,
    setSearch: React.Dispatch<React.SetStateAction<string>>;
    setIsActiveOnly: React.Dispatch<React.SetStateAction<boolean>>;
};