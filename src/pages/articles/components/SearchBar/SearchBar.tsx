import { SearchBarProps } from './SearchBar.interface';
import styles from './searchBar.module.css';

const SearchBar = <T,> ( searchBarProps: SearchBarProps<T> ) => {

    const { search, setSearch, filteredData, isActiveOnly, setIsActiveOnly } = searchBarProps;

    return (

        <div className={ styles.container }>
            <div className={ styles.isActiveContainer }>
                <input
                    type="checkbox"
                    name="isSearchActive"
                    id="isSearchActive"
                    checked={ isActiveOnly }
                    onChange={ () => setIsActiveOnly( !isActiveOnly ) }
                />
                <label htmlFor="isSearchActive">isActiveOnly:&nbsp;</label>
            </div>
            <input
                type="text"
                placeholder='search...'
                value={ search }
                onChange={ e => setSearch( e.target.value ) }
            />
            Total items: { filteredData?.length }
        </div>
    );
};

export default SearchBar;