import { useEffect, useState } from "react";
import { Item } from "../../../shared/interfaces";

const useSearch = <T extends Partial<Item>> ( data: T[] ) => {
    const [ search, setSearch ] = useState( "" );
    const [ isActiveOnly, setIsActiveOnly ] = useState( false );
    const [ filteredData, setFilteredData ] = useState( data );

    const filterByActive = ( array: T[] ) => array.filter( item => item.isActive );

    const filterBySearch = ( array: T[] ) => (
        array.filter( item => Object.values( item )
            .some( value => {
                const regex = /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi;
                if ( !regex.test( value as string ) ) {
                    return value.toString().toLowerCase().includes( search.toLowerCase() );
                }
            } )
        )
    );

    useEffect( () => {
        if ( !data ) return;

        let temp = [ ...data ];

        if ( isActiveOnly ) { temp = filterByActive( temp ); }

        temp = filterBySearch( temp );

        setFilteredData( temp );

    }, [ search, data, isActiveOnly ] );

    return { search, setSearch, isActiveOnly, setIsActiveOnly, filteredData };
};

export default useSearch;