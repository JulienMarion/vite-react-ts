const Home = () => (
    <div style={ { textAlign: 'center' } }>
        <h1 style={ { padding: '1rem' } }>home</h1>
        <h3 style={ { padding: '1rem' } }>Ideas:</h3>
        <ul style={ { display: 'flex', flexDirection: 'column', gap: '1rem' } }>
            <li>make a pagination on articles, like load 20ish, and load moar when scrolling down</li>
            <li>maybe do an infinite scrolling or a page size selector</li>
            <li>add image to articles?</li>
            <li>add a context thematic to switch style, (dark, light) or even go as far as choosing the main color aswell</li>
            <li>do something about formData that put bools at 'on' and not even showing them when they're unchecked</li>
            <li>add feedback when posting articles</li>
            <li>add snackbars maybe</li>
        </ul>
    </div>

);
export default Home;