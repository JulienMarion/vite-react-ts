import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Page404 from '../pages/404/Page404';
import Home from '../pages/home/Home';
import Articles from '../pages/articles/Articles';
import ArticleDetails from '../pages/articleDetails/ArticleDetails';
import Layout from '../shared/layout/Layout';
import AddArticle from '../pages/addArticle/AddArticle';

const Router = () => (
    <BrowserRouter>
        <Routes>
            <Route path={ '/' } element={ <Layout><Home /></Layout> } />
            <Route path={ '/articles/add' } element={ <Layout><AddArticle /></Layout> } />
            <Route path={ '/articles/:id' } element={ <Layout><ArticleDetails /></Layout> } />
            <Route path={ '/articles' } element={ <Layout><Articles /></Layout> } />
            <Route path={ '*' } element={ <Layout><Page404 /></Layout> } />
        </Routes>
    </BrowserRouter>
);

export default Router;