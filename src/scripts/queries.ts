import axios from 'axios';
import { BASE_URL, ENTITIES } from '../config/constants';
import { Article } from '../shared/interfaces';

const axiosInstance = axios.create( {
    baseURL: BASE_URL
} );

export const getArticles = async () => (
    ( await axiosInstance.get( `/${ ENTITIES.ARTICLES }` ) ).data
);

export const createArticle = ( article: Article ) => {
    axiosInstance.post( `/${ ENTITIES.ARTICLES }`, article );
};