import Router from "../router/Router";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

const App = () => (
	<QueryClientProvider client={ queryClient }>
		<Router />
	</QueryClientProvider>
);

export default App;