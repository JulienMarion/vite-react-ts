import { useState, forwardRef, useImperativeHandle } from 'react';
import styles from './Snackbar.module.css';

const Snackbar = forwardRef( ( { message }: { message: string; }, ref ) => {

    const [ isShown, setisShown ] = useState( false );

    useImperativeHandle( ref, () => ( {
        show,
    } ) );

    const show = () => {
        setisShown( true );

        setTimeout( () => {
            setisShown( false );
        }, 3000 );
    };

    const snackbarClasses = () => ( isShown ? `${ styles.snackbar } ${ styles.animated }` : `${ styles.snackbar }` );

    const isSnackbarVisible = () => ( isShown ? 'visible' : 'hidden' );

    return (
        <div
            className={ snackbarClasses() }
            style={ { visibility: isSnackbarVisible() } }
        >
            <div className={ styles.icon }>&#x2713;</div>
            <div className={ styles.message }>{ message }</div>
        </div>
    );
} );

export default Snackbar;