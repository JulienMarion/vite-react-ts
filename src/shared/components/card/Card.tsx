import { HasChildren } from '../../interfaces';
import styles from './Card.module.css';

const Card = ( { children }: HasChildren ) => (
    <article className={ styles.card }>{ children }</article>
);

export default Card;