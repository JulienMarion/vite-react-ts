import { ReactNode } from "react";

export interface ListProps<T> {
    data: T[];
    children: ( item: T ) => ReactNode;
};