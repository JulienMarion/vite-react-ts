import { HasId } from '../../interfaces';
import { ListProps } from './List.interface';
import styles from './List.module.css';

function List<T extends HasId> ( { data, children }: ListProps<T> ) {
    return (
        <ul className={ styles.list }>
            { ( data || [] ).map(
                ( item: T, i: number ) => (
                    <li className={ styles.list_item } key={ item.id || i }>
                        { children( item ) }
                    </li>
                )
            ) }
        </ul>
    );
}

export default List;