import { Link } from "react-router-dom";
import styles from './Navigation.module.css';

const Navigation = () => (
    <nav className={ styles.nav }>
        <ul className={ styles.ul }>
            <li><Link to={ '/' }>Home</Link></li>
            <li><Link to={ '/articles' }>Articles</Link></li>
            <li><Link to={ '/articles/add' }>New Article</Link></li>
            <li><Link to={ '/unknown' }>Unknown</Link></li>
        </ul>
    </nav>
);

export default Navigation;