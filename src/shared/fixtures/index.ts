import { Article, User } from "../interfaces";

const user: User = { name: 'Ju' };

const article: Article = {
    name: 'Calendar',
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.`,
    createdBy: user.name,
    isActive: false,
};

export { user, article };
