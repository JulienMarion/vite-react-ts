import { useEffect, useState } from "react";

const _getSavedValue = ( key: string, initialValue: string | Function ) => {

    const savedValue = JSON.parse( localStorage.getItem( key ) as string );

    if ( savedValue ) return savedValue;

    return ( initialValue instanceof Function ) ? initialValue() : initialValue;
};

const useLocalStorage = ( key: string, initialValue: string | Function ) => {

    const [ value, setValue ] = useState( () => _getSavedValue( key, initialValue ) );

    useEffect( () => {
        localStorage.setItem( key, JSON.stringify( value ) );
    }, [ value ] );

    return [ value, setValue ];
};

export default useLocalStorage;