const useTruncate = () => {

    const _truncate = ( string: string, length: number ) =>
        `${ string.slice( 0, length ) }...`;

    const hasToBeTruncate = ( string: string, length: number ) =>
        string.length > length ? _truncate( string, length ) : string;

    return { hasToBeTruncate };
};

export default useTruncate;