import { ReactNode } from "react";

export interface Item {
    id: string,
    name: string,
    description: string,
    createdAt: string,
    createdBy: string;
    isActive: boolean;
};

export type Article = Omit<Item, "id" | "createdAt">;
export interface User {
    name: string;
}

export interface HasChildren {
    children: ReactNode;
}

export interface HasId {
    id: string;
}
