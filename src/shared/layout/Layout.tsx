import Navigation from "../components/Navigation/Navigation";
import { HasChildren } from "../interfaces";

const Layout = ( { children }: HasChildren ) => (
    <>
        <Navigation />
        { children }
    </>
);

export default Layout;